-- SUMMARY --

The Bonus CSS module enables site administrators to add custom CSS to your theme
without having to create a subtheme. This is preferable on multisites, where 
multiple sites may be accessing the same theme but want specific tinkering. 

For a full description of the module, visit the project page:
  [ENTER URL HERE]

To submit bug reports and feature suggestions, or to track changes:
  [ENTER URL HERE]


-- REQUIREMENTS --

* Drupal installation.

* Permissions to create directories within your sites/[site name] folder.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Create a /css folder in your /sites/[site name] folder.


-- CUSTOMIZATION --

* Place all CSS files into the /sites/[site name]/css folder. BonusCSS will 
  automatically process those CSS files on each page subsequent load.


-- TROUBLESHOOTING --

* None available as of yet.


-- CONTACT --

Current maintainers:
* Eric Sembrat (Webbeh) - http://drupal.org/user/1160608

Previous maintainer:
* David Tomaschik (Matir) - http://drupal.org/user/516662